set nocompatible
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/vundle/
" call vundle#rc()
call vundle#begin()
" alternatively, pass a path where Vundle should install bundles
"let path = '~/some/path/here'
"call vundle#rc(path)

" let Vundle manage Vundle, required
Bundle 'gmarik/vundle'
Bundle 'xolox/vim-misc'
Bundle 'xolox/vim-session'
" Bundle 'kien/ctrlp.vim'
Bundle 'bling/vim-airline'
Bundle 'altercation/vim-colors-solarized'
"Bundle 'scrooloose/nerdcommenter'
" Bundle 'Townk/vim-autoclose'
Bundle 'scrooloose/syntastic'
Bundle 'vim-scripts/YankRing.vim'
Bundle 'jiangmiao/auto-pairs'
Bundle 'mattn/emmet-vim'
Bundle 'sumpygump/php-documentor-vim'
Bundle 'tpope/vim-unimpaired'
" Bundle 'dsdeiz/vim-drupal-snippets'
Bundle 'kien/ctrlp.vim'
Bundle 'tacahiroy/ctrlp-funky'
Bundle 'DavidEGx/ctrlp-smarttabs'
Bundle 'majutsushi/tagbar'
Bundle 'SirVer/ultisnips'
Bundle 'honza/vim-snippets'
" Bundle 'rizzatti/funcoo.vim'
" Bundle 'rizzatti/dash.vim'
" Bundle 'MattesGroeger/vim-bookmarks'
Bundle 'tomtom/tcomment_vim'
Bundle 'Lokaltog/vim-easymotion'
Bundle '2072/PHP-Indenting-for-VIm'
" Plugin 'airblade/vim-gitgutter'

" Bundle 'mileszs/ack.vim'
" Bundle 'git://drupalcode.org/project/vimrc.git', {'rtp': 'bundle/vim-plugin-for-drupal/'}
" Bundle 'tpope/vim-fugitive'

call vundle#end()

syntax enable

let g:solarized_termtrans = 1
set background=dark
colorscheme solarized

"set guifont=Source\ Code\ Pro:h12
" set guifont=Monaco:h12
set guifont=Menlo:h12

set backspace=indent,eol,start

set hidden

set nobackup		" do not keep a backup file, use versions instead
set noswapfile

set history=50    " keep 50 lines of command line history
set ruler   " show the cursor position all the time
set showcmd   " display incomplete commands
set incsearch   " do incremental searching
set showmatch
set hlsearch
set laststatus=2
set autoread

set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set smartindent

set list
set listchars=tab:▸\ ,eol:¬

filetype plugin on

nnoremap <leader><space> :noh<cr>

augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END

set wildmenu

autocmd BufEnter * silent! lcd %:p:h

map <F4> :YRShow<CR>
let g:yankring_history_dir = '~/.vim/yankring'

let g:session_autoload = 'no'

" If you prefer the Omni-Completion tip window to close when a selection is
" made, these lines close it on movement in insert mode or when leaving
" insert mode
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

autocmd BufRead,BufNewFile *.scss set filetype=css


" Set filetype for Drupal PHP files.
autocmd BufRead,BufNewFile *.module set filetype=php
autocmd BufRead,BufNewFile *.install set filetype=php
autocmd BufRead,BufNewFile *.inc set filetype=php
autocmd BufRead,BufNewFile *.profile set filetype=php
autocmd BufRead,BufNewFile *.theme set filetype=php
autocmd BufRead,BufNewFile *.engine set filetype=php
autocmd BufRead,BufNewFile *.test set filetype=php
autocmd BufRead,BufNewFile *.view set filetype=php
autocmd BufRead,BufNewFile *.{info,make,build} set filetype=drini

autocmd BufRead,BufNewFile *.tpl.php set filetype=php.html

let ctrlp_by_filename = 1

nnoremap <silent> <F6> :CtrlP<CR>
nnoremap <silent> <F7> :CtrlPSmartTabs<CR>
nnoremap <silent> <F8> :CtrlPBuffer<CR>
nnoremap <silent> <F9> :CtrlPFunky<CR>

let g:ctrlp_extensions = ['funky','smarttabs']

let g:ctrlp_custom_ignore = {
  \ 'dir': $HOME. '/Sites/blogher/\(.git\|files\|includes\|misc\|modules\|profiles\|safecount\|sites\/default\|sites\/all\/modules\/contrib\|sites\/all\/themes\/\(cheekymonkey\|old\|seven\|snazzy\|zen\)\)',
  \ 'file': '\.txt$\|\.xml$'
  \ }

" strip all trailing whitespace in the current file
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

nnoremap <leader>p p`[v`]=

set guioptions-=L
set guioptions-=r

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Bi-directional find motion
" " Jump to anywhere you want with minimal keystrokes, with just one key
" binding.
" " `s{char}{label}`
" nmap s <Plug>(easymotion-s)
" " or
" " `s{char}{char}{label}`
" " Need one more keystroke, but on average, it may be more comfortable.
nmap s <Plug>(easymotion-s2)
"
"" Turn on case sensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
" "

